/* Global settings */
var settings = {
	debug : false
};
/* Output to console if debug is on */
function echo(message) {
	if (settings.debug) {
		console.log(message);
	}
	;
};
/* IE detection flag */
jQuery.browser = {};
(function() {
	jQuery.browser.msie = false;
	jQuery.browser.version = 0;
	if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
		jQuery.browser.msie = true;
		jQuery.browser.version = RegExp.$1;
	}
})();
/* Global help-vars */
var resizeTimeout;
(function($) {
	var app = {
		'init' : function(e) {
			echo('App init...');
			app.stickyHeader();
			app.sectionScroll();
			app.pushMenu();
			app.skills();
			app.credits();
			app.scrolltoTop();
		    $(document).on("scroll", app.scrollMenu);
			$(window).on('resize', app.onResize);
		},
		'stickyHeader' : function() {
			$(window).bind('scroll', function() {
				if ($(window).scrollTop() > 50) {
					$('header').addClass('sticky');
				} else {
					$('header').removeClass('sticky');
				}
				
			});
		},
		'sectionScroll' : function() {
		    $('.pushmenu a[href^="#"]').on('click', function (e) {
		        e.preventDefault();
		        $(document).off("scroll");
		        $('a').each(function () {
		            $(this).removeClass('active');
		        })
		        $(this).addClass('active');
		        var target = this.hash,
		        menu = target;
		        $target = $(target);
		        $('html, body').stop().animate({
		            'scrollTop': $target.offset().top+2
		        }, 500, 'swing', function () {
		            window.location.hash = target;
		            $(document).on("scroll", app.scrollMenu);
		        });
		    });
		},
		'scrollMenu' : function(){
		    var scrollPos = $(document).scrollTop();
		    $('.pushmenu a').each(function () {
		        var currLink = $(this);
		        var refElement = $(currLink.attr("href"));
		        if ( typeof refElement.position() !== 'undefined') {
			        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
			            $('.pushmenu ul li a').removeClass("active");
			            currLink.addClass("active");
			        }
			        else{
			            currLink.removeClass("active");
			        }
		        }
		    });
		},
		'pushMenu' : function() {
			$('.btn-pushmenu').click(function() {
				$(this).toggleClass('active');
				if ( $( ".btn-pushmenu.active" ).length ) {
					$(".btn-pushmenu span").removeClass("glyphicon-menu-hamburger");
					$(".btn-pushmenu span").addClass("glyphicon-remove");
				}else{
					$(".btn-pushmenu span").removeClass("glyphicon-remove");
					$(".btn-pushmenu span").addClass("glyphicon-menu-hamburger");					
				}
				$('.pushmenu-push').toggleClass('pushmenu-push-toright');
				$('.pushmenu-left').toggleClass('pushmenu-open');
			});
		},
		'skills' : function() {
			  $('.progress-bar:visible').each(function() {
				    var bar_value = $(this).attr('aria-valuenow') + '%';                
				    $(this).animate({ width: bar_value }, { duration: 3000, easing: 'easeOutCirc' });
			  });
			  $('.more-skill:visible').each(function() {
		            $('.chart:visible').easyPieChart({
		                //your configuration goes here
		                easing: 'easeOut',
		                delay: 3000,
		                barColor:'#5ea3c3',
		                trackColor:'#fff',
		                scaleColor: false,
		                lineWidth: 8,
		                size: 140,
		                animate: 2000,
		                onStep: function(from, to, percent) {
		                    this.el.children[0].innerHTML = Math.round(percent);
		                }

		            });
			  });			  
		},
		'credits' : function(){
			if ( $( ".grid" ).length ) {
				$('.grid').masonry({
				    itemSelector: '.grid-item',
				    percentPosition: true,
				    columnWidth: '.grid-item',
                    gutter:0
                }); 
			}
		},
		'scrolltoTop' : function() {
			if ($(window).width() > 999) {
				$(window).scroll(function() {
					if ($(this).scrollTop() > 100) {
						$('.scroll-up-arrow').fadeIn();
					} else {
						$('.scroll-up-arrow').fadeOut();
					}
				});
			}
		},
		'onResize' : function() {
			clearTimeout(resizeTimeout);
			resizeTimeout = setTimeout(function() {

			}, 10);
		}
	};
	$(document).on('ready', app.init);
})(jQuery);
